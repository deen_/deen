// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

import Rails from "@rails/ujs"
import Turbolinks from "turbolinks"
import * as ActiveStorage from "@rails/activestorage"
import "channels"
import "@fortawesome/fontawesome-free/css/all"

Rails.start()
Turbolinks.start()
ActiveStorage.start()

document.addEventListener("turbolinks:load", function() {
  var sns = document.getElementsByClassName("floating-items");
  
  animate_sns(0);

  function animate_sns(i) {
    setTimeout(function(){  
      sns[i].style.display = "block";
      sns[i].classList.add("w3-container");
      sns[i].classList.add("w3-center");
      sns[i].classList.add("w3-animate-top");

      i++;
      if (i<sns.length) animate_sns(i);
    }, 500);
  }
});